package com.hao.gulimall.coupon.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.hao.gulimall.coupon.entity.CouponEntity;
import com.hao.gulimall.coupon.service.CouponService;
import com.hao.common.utils.PageUtils;
import com.hao.common.utils.R;



/**
 * 优惠券信息
 *
 *  @RefreshScope:刷新配置，每次都会从配置中心获取值
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:09:57
 */
@RefreshScope
@RestController
@RequestMapping("coupon/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    //测试nacos配置中心
    @Value("${coupon.user.name}")
    private String name;
    @Value("${coupon.user.age}")
    private Integer age;

    @GetMapping("/user")
    public R testNacosConfig(){
        return R.ok().put("name",name).put("age",age);
    }
    //测试nacos配置中心，end

    /**
     * 调用远程服务测试
     * 会员所拥有的所有优惠券
     *
     * @return {@link R}
     */
    @GetMapping("/member/list")
    public R memeberCoupons(){
        CouponEntity coupon = new CouponEntity();
        coupon.setCouponName("满100，-10");
        return R.ok().put("coupons",Arrays.asList(coupon));
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = couponService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		CouponEntity coupon = couponService.getById(id);

        return R.ok().put("coupon", coupon);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CouponEntity coupon){
		couponService.save(coupon);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CouponEntity coupon){
		couponService.updateById(coupon);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		couponService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
