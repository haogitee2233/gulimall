package com.hao.gulimall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * gulimall优惠券应用程序
 *
 *      //使用 @EnableDiscoveryClient 注解开启服务注册与发现功能
 * @author HaoJiaLi
 * @date 2020/07/04
 */

@SpringBootApplication
@EnableDiscoveryClient
public class GulimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallCouponApplication.class, args);
    }

}
