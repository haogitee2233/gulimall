package com.hao.gulimall.thirdparty;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

@SpringBootTest
class GulimallThirdPartyApplicationTests {

    //    maven依赖从common转移到了第三方整合模块
    @Autowired
    OSSClient ossClient;

    @Test
    void contextLoads() {
    }

    /**
     * 测试阿里云oss上传
     *
     *1、引入oss-starter
     *2、配置文件中配置key,accessKeySecret,endpoint相关信息即可
     *3、使用 ossCLient进行相关操作
     *
     * @throws Exception 异常
     */
    @Test
    public void testUpload() throws Exception{
        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String endpoint = "oss-cn-shanghai.aliyuncs.com";
//    // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
//        String accessKeyId = "LTAI4G2GMma9JvHkX2AtomLB";
//        String accessKeySecret = "DEV5wg0ZkQnoHw0LoJrKj8v9SKh3BU";

    // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

    // 创建PutObjectRequest对象。
        PutObjectRequest putObjectRequest = new PutObjectRequest("gulimall-kiana", "bili3.jpg",
                new File("C:\\Users\\HaoJiaLi\\Pictures\\Turing\\0f67f7c2fb.jpg"));

    // 如果需要上传时设置存储类型与访问权限，请参考以下示例代码。
    // ObjectMetadata metadata = new ObjectMetadata();
    // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
    // metadata.setObjectAcl(CannedAccessControlList.Private);
    // putObjectRequest.setMetadata(metadata);

//    // 上传文件。
        ossClient.putObject(putObjectRequest);

    // 关闭OSSClient。
        ossClient.shutdown();
    }

}
