package com.hao.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * gulimall网关应用程序
 *
 * @author HaoJiaLi
 * @date 2020/07/05
 *      @SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
 *          引入了mybatis依赖，不使用数据库配置，需要排除，否则报错
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GulimallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallGatewayApplication.class, args);
    }

}
