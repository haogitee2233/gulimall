# 谷粒商城

教程：[谷粒商城](https://www.bilibili.com/video/BV1rK4y1C7fv)



## 1.分布式基础概念

1. 微服务
   1. 微服务架构风格，就像是把一个单独的应用程序开发为一套小服务，每个小服务运行在自己的进程中，并使用轻量级机制通信，通常是HTTP API。

2. 这些服务围绕业务能力来构建，并通过完全自动化部署机制来独立部署
3. 这些服务使用不同的的变成语言书写
4. 使用不同的数据存储技术
5. 保持最低限度的集中式管理
6. 简而言之：拒绝大型单体应用，基于业务边界进行服务微化拆分，各个服务独立部署运行

### 2. 集群，分布式，节点

1. 集群是个物理形态，分布式是个工作方式
   	1. “分布式是若干独立计算机的集合，这些计算机对于用户来说就像是个单个相关系统，分布式系统（distributed system），是建立在网络之上的软件系统”
   	2. 分布式是指将不同的业务，分布在不同的地方
   	3. 集群是指，将几台服务器集中在一起，实现同一业务
   	4. 分布式中的每一个节点，都可以做集群，集群不一定是分布式的
   2. 节点：集群中的一个服务器

### 3. 远程调用

1. 在分布式中，各个服务可能处于不同主机，但是服务之间需要互相调用，就是远程调用

2. SpringCloud中使用HTTP+JSON的方式完成远程调用

### 4. 负载均衡

```
1. 均衡使用每一个服务器，提升网站的健壮性
```

2. 常见的负载均衡算法：
   	1. 轮询：为第一个请求选择第一个后端服务器，然后按顺序向后选择，直到最后一个，循环
   	2. 最小连接：优先选择连接数最少，也就是压力最小的服务器，在会话较长的情况下可以考虑
   	3. 散列：根据请求源的IP的散列（hash），选择要使用的服务器。这种方式可以一定程度上保证用户能连接到相同的服务器，如果应用需要处理状态而要求用户能连接到和之前相同的服务器，可以考虑使用

### 5. 服务注册，发现&注册中心

1. 服务之间互相调用，但并不知道要调用的服务在哪里，状态如何，能否正常使用。因此，需要注册中心

### 6. 配置中心

1. 每一个服务都有大量的配置，并且每个服务都可能部署在多台机器上，我们需要经常变换配置，可以让每个服务在配置中心获取自己的配置

2. 配置中心用来集中管理微服务的配置信息

### 7. 服务熔断，服务降级

1. 在微服务架构中，微服务之间通过网络进行通信，存在相互依赖，当其中一个服务不可用时，有可能造成雪崩效应，为防止这种情况，必须要有容错机制来保护服务

2. 服务熔断：

   1. 设置服务的超时，当被调用的服务经常失败到达某个阈值，开启断路保护机制，后来的请求不再调用这个服务。本地直接返回默认数据

      服务降级：

   1. 运维期间，当系统处于高峰期，系统资源紧张，可以让非核心业务降级运行
   2. 降级：某些服务不处理，或者简单处理：抛异常，返回null，调用Mock数据，调用Fallback处理逻辑等

### 8. API网关

​	1. 在微服务架构中，API Gateway作为整体架构的重要组件，抽象了微服务中都需要的公共功能，同时提供了客户端负载均衡，服务自动熔断，灰度发布，统一认证，限流流控，日志统计等丰富功能，帮助我们解决很多API管理难题。



## 2. 架构图

​	<img src="D:\program\Typora\image\谷粒商城架构图.png" style="zoom:100%;" />

## 3. 微服务划分图

![微服务划分图](D:\program\Typora\image\微服务划分图.png)





## 4.创建项目微服务

```
	- 商品服务-product
	- 仓储服务-order
	- 订单服务-ware
	- 优惠券服务-coupon
	- 用户服务-member
```

共同点：

```
- 引入web、openfeign
- 服务报名：com.hao.gulimall.xxx
- 模块名：gulimall-xxx
```

1. 整合MyBatis-Plus

   1. 导入依赖

   2. 配置

      1. 配置数据源

         1. 导入数据库驱动
         2. 配置数据源信息

      2. 配置MyBatis-Plus信息

         1. 入口方法使用注解

            1. ```java
               @MapperScan("com.hao.gulimall.product.dao")
               @SpringBootApplication
               public class GulimallProductApplication {
               
                   public static void main(String[] args) {
                       SpringApplication.run(GulimallProductApplication.class, args);
                   }
               
               }
               ```

   ```yaml
   #数据源信息
   spring:
     datasource:
       username: root
       password: root
       url: jdbc:mysql://192.168.56.10:3306/gulimall_pms?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai
       driver-class-name: com.mysql.jdbc.Driver
   
   #配置文件位置
   mybatis-plus:
     mapper-locations: classpath:/mapper/**/*.xml
   #  配置主键自增
     global-config:
       db-config:
         id-type: auto
   ```

   

## 5. SpringCloud组件

SpringCloud Alibaba：

github：https://github.com/alibaba/spring-cloud-alibaba/blob/master/README-zh.md

1. Nacos：
   		1. 注册中心，服务发现/注册
   		2. 配置中心，动态配置管理
   	2. Sentinel：服务容错，限流，降级，熔断
   	3. Seata：原Fescar，分布式事务解决方案

SpringCloud：

​	1.Ribbon：负载均衡

2. Feign：声明式HTTP客户端，调用远程服务
3. GateWay：API网关，webflux编程模式
4. Sleuth：调用链监控

### 1. 使用Nacos注册服务

​	github：https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-discovery-example/readme-zh.md

### 2. 使用Feign远程调用服务

1. 引入OpenFeign依赖

   ```xml
   <dependency>
       <groupId>org.springframework.cloud</groupId>
       <artifactId>spring-cloud-starter-openfeign</artifactId>
   </dependency>
   ```

   

2. 声明一个接口，告诉SpringCloud这个接口需要调用远程服务，并声明每个方法需要调用的远程服务对应的方法

   ```java
   //去gulimall-coupon服务中，调用/coupon/coupon/member/list路径的memeberCoupons()方法
   @FeignClient("gulimall-coupon")
   public interface CouponFeignService {
   
       @GetMapping("/coupon/coupon/member/list")
       public R memeberCoupons();
   
   }
   ```

   3. 入口类使用@EnableFeignClients("com.hao.gulimall.member.feign")，开启调用远程服务功能



### 3.使用Nacos配置中心

​	github：https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-config-example/readme-zh.md

​	使用细节：

```
1. 需要给配置中心默认添加一个 ： 数据集（Data Id） gulimall-coupon.properties，默认为：应用名.properties
```

2. 在配置中心中给 该properties添加配置
3. 动态获取配置
   	1. @RefreshScope：动态获取刷新配置
   	2. @Value("${user.name}")，获取配置
   	3. 如果配置中心和当前应用自己的配置文件配置了相同的属性，优先使用配置中心的配置
   4. 命名空间：用来进行配置隔离，不同的命名空间，可以使用相同的Group或Data ID的配置，
   5. 默认为：public(保留空间)
           	2. 开发测试环境和生产环境的隔离
      3. 每个微服务之间互相隔离配置
   6. 配置集：一组相关或不相关的配置项的集合。系统中，一个配置文件通常就是一个配置集，包含了系统的各项配置，例如：数据源，线程池，日志级别
   7. 配置集ID：类似配置文件名，nacos中的配置的Data ID
   8. 配置分组：配置集的组别，默认都是DEFAULT_GROUP
   9. 拆分配置，在配置中心使用多配置集
   10. 微服务的任何配置，都可以放在配置中心
            	2. 只需要在bootstrap.properties中，指定配置文件信息即可
   11. 使用规则：每个微服务创建自己的命名空间，使用配置分组区分环境，dev开发，test测试，prod生产

```properties
#配置文件名：bootstrap.properties
#当前应用名
spring.application.name=gulimall-coupon
#配置Nacos Server地址，名字，和服务注册功能一样
spring.cloud.nacos.config.server-addr=192.168.1.2:8848
#使用命名空间ID，来使用指定命名空间，默认使用public(保留空间)
spring.cloud.nacos.config.namespace=e340a61f-3191-4260-b035-b71f53402e2f
#指定使用的组别，默认DEFAULT_GROUP
spring.cloud.nacos.config.group=dev

#加载多配置集，拆分配置文件。指定文件名，分组，是否刷新
spring.cloud.nacos.config.extension-configs[0].data-id=datasource.yml
spring.cloud.nacos.config.extension-configs[0].group=dev
spring.cloud.nacos.config.extension-configs[0].refresh=true

spring.cloud.nacos.config.extension-configs[1].data-id=mybatis.yml
spring.cloud.nacos.config.extension-configs[1].group=dev
spring.cloud.nacos.config.extension-configs[1].refresh=true

spring.cloud.nacos.config.extension-configs[2].data-id=other.yml
spring.cloud.nacos.config.extension-configs[2].group=dev
spring.cloud.nacos.config.extension-configs[2].refresh=true

```



















