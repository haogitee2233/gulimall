package com.hao.gulimall.product;

import com.hao.gulimall.product.entity.BrandEntity;
import com.hao.gulimall.product.service.BrandService;
import com.hao.gulimall.product.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@Slf4j
@SpringBootTest
class GulimallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Test
    void contextLoads() {
        BrandEntity brand = new BrandEntity();
        brand.setDescript("搞基搞机111");
        brand.setName("一加");
        brandService.save(brand);
        System.out.println("保存成功");
    }

    @Test
    void test1(){
        Long[] catelogPath = categoryService.findCatelogPath((long) 225);
        log.info("路径：{}", Arrays.asList(catelogPath));
    }


}
