package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存产品属性
     *
     * @param collect 收集
     */
    void saveProductAttr(List<ProductAttrValueEntity> collect);

    /**
     * 根据spuid，得到对应的attr基本信息
     *
     * @param spuId spu id
     * @return {@link List<ProductAttrValueEntity>}
     */
    List<ProductAttrValueEntity> baseAttrListForSpu(Long spuId);
}

