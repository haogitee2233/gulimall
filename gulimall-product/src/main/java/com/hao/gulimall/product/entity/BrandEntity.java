package com.hao.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hao.common.valid.AddGroup;
import com.hao.common.valid.ListValue;
import com.hao.common.valid.UpdateGroup;
import com.hao.common.valid.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 品牌
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 品牌id
     * 使用分组校验规则
     * 没有指定分组的检验注解，在分组检验中不生效，只会在未分组的注解生效
     */
    @NotNull(message = "更新品牌必须指定ID", groups = {UpdateGroup.class})
    @Null(message = "新增品牌不能指定ID", groups = {AddGroup.class})
    @TableId(type = IdType.AUTO)
    private Long brandId;
    /**
     * 品牌名
     */
    @NotBlank(message = "品牌名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    /**
     * 品牌logo地址
     *
     * @NotBlank(groups = {AddGroup.class}):如果是新增，那么使用AddGroup检验组别
     * @URL(message = "logo地址必须是url",groups = {AddGroup.class,UpdateGroup.class})：
     * 修改时可以不携带，如果带了，触发@URL校验
     * 下面同理
     */
    @NotBlank(groups = {AddGroup.class})
    @URL(message = "logo地址必须是url", groups = {AddGroup.class, UpdateGroup.class})
    private String logo;
    /**
     * 介绍
     */
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     *
     * @ListValue(vals = {0,1}):使用自定义的异常注解，并指定可用的值
     */
    @NotNull(groups = {AddGroup.class, UpdateStatusGroup.class})
    @ListValue(vals = {0, 1}, groups = {AddGroup.class, UpdateStatusGroup.class})
    private Integer showStatus;
    /**
     * 检索首字母
     */
    @NotEmpty(groups = {AddGroup.class})
    @Pattern(regexp = "^[a-zA-Z]$", message = "检索首字母必须是单个英文字母", groups = {AddGroup.class, UpdateGroup.class})
    private String firstLetter;
    /**
     * 排序
     */
    @NotNull(groups = {AddGroup.class})
    @Min(value = 0, message = "排序号不能小于0", groups = {AddGroup.class, UpdateGroup.class})
    private Integer sort;

}
