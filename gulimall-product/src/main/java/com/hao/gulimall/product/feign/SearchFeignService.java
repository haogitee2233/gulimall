package com.hao.gulimall.product.feign;

import com.hao.common.to.es.SkuEsModel;
import com.hao.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName SearchFeignService.java
 * @createTime 2021/3/23,21:04
 * @Description
 */
@FeignClient("gulimall-search")
public interface SearchFeignService {

    @PostMapping("/search/save/product")
    R prodUp(@RequestBody List<SkuEsModel> skuEsModels);
}
