package com.hao.gulimall.product.dao;

import com.hao.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
