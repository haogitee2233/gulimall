package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    public void removeMenuByIds(List<Long> asList);

    /**
     * 找到catelogId完整路径
     * 格式：[父/子/孙]
     * @param catelogId catelog id
     */
    Long[] findCatelogPath(Long catelogId);

    /**
     * 更新级联，自身以及关联表
     *
     * @param category 类别
     */
    void updateCascade(CategoryEntity category);
}

