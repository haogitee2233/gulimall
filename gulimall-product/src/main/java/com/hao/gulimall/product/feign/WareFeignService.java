package com.hao.gulimall.product.feign;

import com.hao.common.to.SkuHasStockVo;
import com.hao.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName WareFeignService.java
 * @createTime 2021/3/23,19:58
 * @Description
 */
@FeignClient("gulimall-ware")
public interface WareFeignService {

    /**
     * 查询sku是否有库存
     *
     * @param skuIds sku id
     * @return {@link R<List<SkuHasStockVo>>}
     */
    @PostMapping("ware/waresku/getSkuHasStock")
    R<List<SkuHasStockVo>> getSkuHasStock(@RequestBody List<Long> skuIds);
}
