package com.hao.gulimall.product.controller;

import com.hao.common.utils.PageUtils;
import com.hao.common.utils.R;
import com.hao.common.valid.AddGroup;
import com.hao.common.valid.UpdateGroup;
import com.hao.common.valid.UpdateStatusGroup;
import com.hao.gulimall.product.entity.BrandEntity;
import com.hao.gulimall.product.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 品牌
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 11:06:26
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId) {
        BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     *
     * @Valid: 启用实体类的数据校验注解,后面紧跟BindingResult result，接收出现异常的实体类
     *@Validated({AddGroup.class})：启用校验注解，并指定分组校验
     *
     */
    @RequestMapping("/save")
    public R save(/*@Valid*/ @Validated({AddGroup.class}) @RequestBody BrandEntity brand/*,BindingResult result*/) {
        //自定义错误返回对象
        //换用统一异常处理
//        if(result.hasErrors()){
//            Map<String,String> map = new HashMap<>();
//            //获取校验错误结果
//            result.getFieldErrors().forEach((item) ->{
//                //错误提示和属性名
//                String message = item.getDefaultMessage();
//                String field = item.getField();
//                map.put(field,message);
//            });
//            R.error(400,"提交数据不合法").put("data",map);
//        }else{
//            brandService.save(brand);
//        }
        brandService.save(brand);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody BrandEntity brand) {
        //更新自身，以及关联的表
        brandService.updateDetail(brand);

        return R.ok();
    }

    /**
     * 更新状态
     *
     * @param brand 品牌
     * @return {@link R}
     */
    @RequestMapping("/update/status")
    public R updateStatus(@Validated({UpdateStatusGroup.class}) @RequestBody BrandEntity brand) {
        brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] brandIds) {
        brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
