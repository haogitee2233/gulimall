package com.hao.gulimall.product.feign;

import com.hao.common.to.SkuReductionTo;
import com.hao.common.to.SpuBoundTo;
import com.hao.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName SpuFeignService.java
 * @Description
 * @createTime 2020/7/30,19:40
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);

    @PostMapping("/coupon/skufullreduction/saveinfo")
    R saveSkuReduction(SkuReductionTo skuReductionTo);
}
