package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.SkuInfoEntity;
import com.hao.gulimall.product.vo.SpuSaveVo;

import java.util.List;
import java.util.Map;

/**
 * sku信息
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存sku信息
     *
     * @param skuInfoEntity sku信息实体
     */
    void saveSkuInfo(SkuInfoEntity skuInfoEntity);

    /**
     * 通过条件查询页面
     *
     * @param params 参数个数
     * @return {@link PageUtils}
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 通过spuid，得到对应的sku信息
     *
     * @param spuId spu id
     * @return {@link List<SkuInfoEntity>}
     */
    List<SkuInfoEntity> getSkusBySpuId(Long spuId);
}

