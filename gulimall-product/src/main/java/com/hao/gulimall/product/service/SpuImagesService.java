package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.SpuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * spu图片
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存图片集
     *
     * @param id     id
     * @param images 图片
     */
    void saveImages(Long id, List<String> images);
}

