package com.hao.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import com.hao.gulimall.product.vo.AttrRespVo;
import com.hao.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hao.gulimall.product.entity.AttrEntity;
import com.hao.gulimall.product.service.AttrService;
import com.hao.common.utils.PageUtils;
import com.hao.common.utils.R;


/**
 * 商品属性
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 11:06:26
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;


    /**
     * 基地atr列表
     *
     * @param params    参数个数
     * @param catelogId
     * @param type  {attrType}：base:规格参数 sale：销售属性
     * @return {@link R}
     */
    @GetMapping("/{attrType}/list/{catelogId}")
    public R baseAtrList(@RequestParam Map<String, Object> params, @PathVariable("catelogId") Long catelogId
    ,@PathVariable("attrType")String type) {

        PageUtils page = attrService.queryBaseAttrPage(params,catelogId,type);
        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId) {

        //AttrEntity attr = attrService.getById(attrId);
        AttrRespVo attrRespVo = attrService.getAttrInfo(attrId);

        return R.ok().put("attr", attrRespVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrVo attr) {
        attrService.saveAttrAndGroup(attr);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrVo attr) {
        attrService.updateAttrVo(attr);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrIds) {
        attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
