package com.hao.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * gulimall产品应用
 *
 * @author HaoJiaLi
 * @date 2020/07/11
 */
@EnableFeignClients(basePackages = "com.hao.gulimall.product.feign")
@EnableDiscoveryClient
@MapperScan("com.hao.gulimall.product.dao")
@SpringBootApplication
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
