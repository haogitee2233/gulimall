package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.AttrEntity;
import com.hao.gulimall.product.vo.AttrRespVo;
import com.hao.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存属性信息，以及和属性分组的关联
     *
     * @param attr attr
     */
    void saveAttrAndGroup(AttrVo attr);

    /**
     * 查询商品规格参数列表
     *
     * @param params    参数个数
     * @param catelogId catelog id
     * @param type
     * @return {@link PageUtils}
     */
    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String type);

    /**
     * 得到attr信息，返回包含属性分组的路径
     *
     * @param attrId attr id
     * @return {@link AttrRespVo}
     */
    AttrRespVo getAttrInfo(Long attrId);

    /**
     * 更新AttrVo，属性及其分组分类信息
     *
     * @param attr attr
     */
    void updateAttrVo(AttrVo attr);


    /**
     * 根据分组id，找到组内关联的属性
     *
     * @param attrgroupId attrgroup id
     * @return {@link List<AttrEntity>}
     */
    List<AttrEntity> getRelationAttr(Long attrgroupId);

    /**
     * 根据分组id，找到组内未被关联的属性，用于新建分组和属性的关联
     *
     * @param params      参数个数
     * @param attrgroupId attrgroup id
     * @return {@link PageUtils}
     */
    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);

    /**
     * 根据attrids，得到其中可以被检索的规格属性id
     *
     * @param attrIds attr id
     * @return {@link List<Long>}
     */
    List<Long> selectSearchAtts(List<Long> attrIds);
}

