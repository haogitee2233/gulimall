package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.BrandEntity;
import com.hao.gulimall.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:47
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    /**
     * 查询页面
     *
     * @param params 参数个数
     * @return {@link PageUtils}
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存完整信息细节
     *
     * @param categoryBrandRelation 类别品牌关系
     */
    void saveDetail(CategoryBrandRelationEntity categoryBrandRelation);

    /**
     * 通过品牌id，更新品牌name
     *
     * @param brandId 品牌标识
     * @param name    的名字
     */
    void updateBrand(Long brandId, String name);

    /**
     * 通过分类id，更新类别name
     *
     * @param catId 猫id
     * @param name  的名字
     */
    void updateCategory(Long catId, String name);

    List<BrandEntity> getBrandsByCatId(Long catId);
}

