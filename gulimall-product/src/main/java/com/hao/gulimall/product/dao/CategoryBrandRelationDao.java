package com.hao.gulimall.product.dao;

import com.hao.gulimall.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:47
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

    /**
     * 更新类别
     *
     * @param catId 猫id
     * @param name  的名字
     *                 @Param("catId"): 别名，方便在CategoryBrandRelationDao.xml中取值赋值
     */
    void updateCategory(@Param("catId") Long catId, @Param("name") String name);
}
