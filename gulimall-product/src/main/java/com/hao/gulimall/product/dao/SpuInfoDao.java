package com.hao.gulimall.product.dao;

import com.hao.gulimall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * spu信息
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {

    /**
     * 更新spu状态
     *
     * @param spuId spu id
     * @param code  代码
     */
    void updateSpuStatus(@Param("spuId") Long spuId, @Param("status") int code);
}
