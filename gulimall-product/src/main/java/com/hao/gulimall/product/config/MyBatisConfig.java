package com.hao.gulimall.product.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName MyBatisConfig.java
 * @Description
 * @createTime 2020/7/23,20:15
 */
@Configuration
//开启事务
@EnableTransactionManagement
@MapperScan("com.hao.gulimall.product.dao")
public class MyBatisConfig {
    //引入分页插件
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        //请求页面大于最大页时，true：回到首页，false：继续请求。默认false
        paginationInterceptor.setOverflow(true);
        //设置最大单页数量，默认500条， -1：不受限制
        paginationInterceptor.setLimit(1000);

        return paginationInterceptor;
    }
}
