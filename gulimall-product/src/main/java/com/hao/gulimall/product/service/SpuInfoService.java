package com.hao.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.product.entity.SpuInfoEntity;
import com.hao.gulimall.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 10:30:46
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 新增商品
     *
     * @param vo 签证官
     */
    void saveSpuInfo(SpuSaveVo vo);

    /**
     * 保存spu基础信息
     *
     * @param infoEntity 信息实体
     */
    void saveBaseSpuInfo(SpuInfoEntity infoEntity);

    /**
     * 通过条件查询页面
     *
     * @param params 参数个数
     * @return {@link PageUtils}
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 商品上架发布
     *
     * @param spuId spu id
     */
    void spuUp(Long spuId);
}

