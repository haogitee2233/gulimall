package com.hao.gulimall.member.feign;

import com.hao.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName CouponFeignService.java
 * @Description
 * @createTime 2020/7/4,19:37
 *
 *  去gulimall-coupon服务中，调用/coupon/coupon/member/list路径的memeberCoupons()方法
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @GetMapping("/coupon/coupon/member/list")
    public R memeberCoupons();

}
