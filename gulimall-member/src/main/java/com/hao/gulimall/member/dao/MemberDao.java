package com.hao.gulimall.member.dao;

import com.hao.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:20:03
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
