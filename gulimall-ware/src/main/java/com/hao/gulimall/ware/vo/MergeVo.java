package com.hao.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName MergeVo.java
 * @Description
 * @createTime 2020/8/2,16:54
 *      合并采购需求为采购单
 */
@Data
public class MergeVo {

    private Long purchaseId;
    private List<Long> items;
}
