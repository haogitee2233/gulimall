package com.hao.gulimall.ware.dao;

import com.hao.gulimall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:47:17
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
