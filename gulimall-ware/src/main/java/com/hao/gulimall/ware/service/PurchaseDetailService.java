package com.hao.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.ware.entity.PurchaseDetailEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:47:17
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据采购单id,查出所有采购需求
     *
     * @param id 采购单id
     * @return {@link List<PurchaseDetailEntity>} 所有采购需求
     */
    List<PurchaseDetailEntity> listDetailByPurchaseId(Long id);
}

