package com.hao.gulimall.ware.dao;

import com.hao.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:47:17
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
