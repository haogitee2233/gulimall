package com.hao.gulimall.ware.dao;

import com.hao.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:47:17
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
