package com.hao.gulimall.ware.dao;

import com.hao.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存
 * 
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:47:17
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    /**
     * 查询数据库，是否有sku：库存总数 - 锁定库存数
     *
     * @param skuId sku id
     * @return {@link Long}
     */
    Long getSkuStock(@Param("skuId") Long skuId);
}
