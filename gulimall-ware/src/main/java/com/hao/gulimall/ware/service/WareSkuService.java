package com.hao.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hao.common.to.SkuHasStockVo;
import com.hao.common.utils.PageUtils;
import com.hao.gulimall.ware.entity.WareSkuEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author haojiali
 * @email 1462683411@qq.com
 * @date 2020-07-04 13:47:17
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据skuId，查询该sku是否有库存
     *
     * @param skuIds sku id
     * @return {@link List<SkuHasStockVo>}
     */
    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);
}

