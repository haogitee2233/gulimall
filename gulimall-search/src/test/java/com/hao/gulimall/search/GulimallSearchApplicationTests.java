package com.hao.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.hao.gulimall.search.config.ESconfig;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Map;

@SpringBootTest
class GulimallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @Test
    public void contextLoads() {
        System.out.println(client);
    }

    //测试检索
    @Test
    public void testSearch() throws IOException {
        //创建search请求
        SearchRequest searchRequest = new SearchRequest();
        //指定一个或多个要检索的索引
        searchRequest.indices("bank");
        //指定DSL语句,查询所有人的年龄段，并得到每个年龄段的平均工资
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.size(10);
        //创建聚合查询条件
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(100);
        //聚合内的二次聚合条件
        AvgAggregationBuilder balanceAvg = AggregationBuilders.avg("balanceAvg").field("balance");
        TermsAggregationBuilder aggs = ageAgg.subAggregation(balanceAvg);
        searchSourceBuilder.aggregation(aggs);
        //search请求设置查询语句
        //System.out.println(searchSourceBuilder.toString());
        searchRequest.source(searchSourceBuilder);
        //执行查询
        SearchResponse searchResponse = client.search(searchRequest, ESconfig.COMMON_OPTIONS);
        //System.out.println(searchResponse.toString());
        //json封装为map，方便获取数据
        //Map map = JSON.parseObject(searchResponse.toString(), Map.class);
        //System.out.println(map);
        //分析hit数据，可以封装为javabean对象，进一步处理
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit:searchHits){
            //System.out.println(hit.getSourceAsString());
            //hit.getIndex();
            //hit.getType();
            //hit.getId();
        }
        //分析聚合数据
        Aggregations aggregations = searchResponse.getAggregations();
        Terms ageAgg1 = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg1.getBuckets()) {
            System.out.println("年龄="+bucket.getKeyAsString()+"的人有："+bucket.getDocCount());
            Avg balanceAvg1 = bucket.getAggregations().get("balanceAvg");
            System.out.println("平均收入为："+balanceAvg1.getValue());
        }
    }

    //测试保存更新
    @Test
    public void testIndex() throws IOException {
        //new请求对象
        IndexRequest index = new IndexRequest("user");
        //指定id，或者自动生成
        index.id("2");
        //要保存的数据，方式一：直接key-value对
        //index.source("userName","lisa","age","24","gender","女");
        //方式二：存储json对象
        user user = new user();
        user.setUserName("keqing");
        user.setGender("女");
        user.setAge(22);
        String jsonString = JSON.toJSONString(user);
        index.source(jsonString, XContentType.JSON);
        //执行保存
        IndexResponse indexResponse = client.index(index, ESconfig.COMMON_OPTIONS);
        System.out.println(indexResponse);
    }

}
@Data
class user{
    private String userName;
    private int age;
    private String gender;
}
