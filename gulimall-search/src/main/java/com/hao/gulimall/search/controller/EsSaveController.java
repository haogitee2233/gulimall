package com.hao.gulimall.search.controller;

import com.hao.common.exception.BizCodeEnume;
import com.hao.common.to.es.SkuEsModel;
import com.hao.common.utils.R;
import com.hao.gulimall.search.service.ProdSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName EsSaveController.java
 * @createTime 2021/3/23,20:32
 * @Description ES保存
 */
@Slf4j
@RestController
@RequestMapping("/search/save")
public class EsSaveController {

    @Autowired
    ProdSaveService esSaveService;

    /**
     * 商品上架服务
     *
     * @return {@link R}
     */
    @PostMapping("/product")
    public R prodUp(@RequestBody List<SkuEsModel> skuEsModels) {
        Boolean b = false;
        try {
            b = esSaveService.prodUp(skuEsModels);
        } catch (IOException e) {
            log.error("EsSaveController商品上架错误：{}", e);
            return R.error(BizCodeEnume.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnume.PRODUCT_UP_EXCEPTION.getMsg());
        }
        if (b) {
            return R.ok();
        } else {
            return R.error(BizCodeEnume.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnume.PRODUCT_UP_EXCEPTION.getMsg());
        }
    }
}
