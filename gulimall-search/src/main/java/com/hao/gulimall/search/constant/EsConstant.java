package com.hao.gulimall.search.constant;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName EsConstant.java
 * @createTime 2021/3/23,20:40
 * @Description
 */
public class EsConstant {

    /**
     * sku数据在ES中的索引
     */
    public static final String PRODUCT_INDEX = "product";

}
