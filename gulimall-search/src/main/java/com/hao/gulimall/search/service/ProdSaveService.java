package com.hao.gulimall.search.service;

import com.hao.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName EsSaveService.java
 * @createTime 2021/3/23,20:35
 * @Description 保存商品信息
 */
public interface ProdSaveService {

    /**
     * 商品上架服务
     *
     * @param skuEsModels sku es模型
     */
    Boolean prodUp(List<SkuEsModel> skuEsModels) throws IOException;
}
