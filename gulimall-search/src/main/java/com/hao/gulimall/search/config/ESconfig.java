package com.hao.gulimall.search.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName ESconfig.java
 * @createTime 2021/3/20,16:25
 * @Description     ES自定义配置类
 */

@Configuration
public class ESconfig {

    /**
     * ES请求的通用规则
     */
    public static final RequestOptions COMMON_OPTIONS;
    static {
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
        COMMON_OPTIONS = builder.build();
    }


    /**
     * 创建RestHighLevelClient，放入Bean容器
     *
     * @return {@link RestHighLevelClient}
     */
    @Bean
    public RestHighLevelClient esRestClien(){
        RestClientBuilder builder = RestClient.builder(new HttpHost("192.168.56.10", 9200, "http"));
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(builder);
        return restHighLevelClient;
    }
}
