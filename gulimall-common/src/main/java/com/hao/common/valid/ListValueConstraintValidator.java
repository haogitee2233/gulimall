package com.hao.common.valid;

import javax.validation.ConstraintValidator;
import java.util.HashSet;
import java.util.Set;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName ListValueConstraintValidator.java
 * @Description
 * @createTime 2020/7/20,20:22
 *   自定义校验注解的校验实现类
 *      ConstraintValidator<ListValue,Integer>:
 *          ListValue:校验注解
 *          Integer：要校验的类型
 */
public class ListValueConstraintValidator implements ConstraintValidator<ListValue,Integer> {

    private Set<Integer> set = new HashSet<>();
    /**
     * 初始化方法
     *
     * @param constraintAnnotation 自定义的校验注解
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {
        //给定的正确的值
        int[] vals = constraintAnnotation.vals();
        for (int val : vals) {
            set.add(val);
        }
    }
    /**
     * 判断是否校验成功
     *
     * @param integer                    要校验的值
     * @param constraintValidatorContext 约束验证器内容
     * @return boolean
     */
    @Override
    public boolean isValid(Integer integer, javax.validation.ConstraintValidatorContext constraintValidatorContext) {
        //判断要校验的值，是否在给定正确的值内
        return set.contains(integer);
    }
}
