package com.hao.common.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName ListValue.java
 * @Description
 * @createTime 2020/7/20,20:12
 * 自定义校验注解
 *      @Target:注解的使用范围
 *      @Retention：使用时机
 *      @Constraint(validatedBy = { ListValueConstraintValidator.class })：
 *          指定注解的实现类
 *
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = { ListValueConstraintValidator.class })
public @interface ListValue {
    String message() default "{com.hao.common.valid.ListValue.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] vals() default {};
}
