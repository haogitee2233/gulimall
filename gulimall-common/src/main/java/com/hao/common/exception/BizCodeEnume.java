package com.hao.common.exception;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName BizCodeEnume.java
 * @Description
 * @createTime 2020/7/20,19:41
 * 枚举类，业务异常状态码
 */
public enum BizCodeEnume {
    UNKNOW_EXCEPTION(10000,"系统未知异常"),
    VAILD_EXCEPTION(10001,"参数格式检验失败"),
    PRODUCT_UP_EXCEPTION(10000,"商品上架异常");

    private int code;
    private String msg;
    BizCodeEnume(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
