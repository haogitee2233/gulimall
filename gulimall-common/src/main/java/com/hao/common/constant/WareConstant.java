package com.hao.common.constant;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName WareConstant.java
 * @Description
 * @createTime 2020/8/2,17:01
 *      库存常量类
 */
public class WareConstant {

    //采购单
    public enum PurchaseStatusEnum{
        /**
         * 新建
         */
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),
        FINISH(3,"已完成"),
        HASERROR(4,"有异常");
        private int code;
        private String msg;

        PurchaseStatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    /**
     * 采购需求
     *
     * @author HaoJiaLi
     * @date 2020/08/02
     */
    public enum PurchaseDetailStatusEnum{
        /**
         * 新建
         */
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),
        FINISH(3,"已完成"),
        HASERROR(4,"采购失败");
        private int code;
        private String msg;

        PurchaseDetailStatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
