package com.hao.common.constant;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName ProductConstant.java
 * @Description
 * @createTime 2020/7/25,20:55
 *      商品常量类
 *
 */
public class ProductConstant {

    public enum AttrEnum{
        /**
         * 规格参数
         */
        ATTR_TYPE_BASE(1,"规格参数"),
        ATTR_TYPE_SALE(0,"销售属性");
        private int code;
        private String msg;

        AttrEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    public enum StatusEnum{
        /**
         * 商品上架状态参数
         */
        NEW_SPU(0,"新建"),
        UP_SPU(1,"上架"),
        DOWN_SPU(2,"下架");
        private int code;
        private String msg;

        StatusEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }


}
