package com.hao.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName MemberPrice.java
 * @Description
 * @createTime 2020/7/30,20:05
 */
@Data
public class MemberPrice {
    private Long id;
    private String name;
    private BigDecimal price;
}
