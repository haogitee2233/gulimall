package com.hao.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName SpuBoundTo.java
 * @Description
 * @createTime 2020/7/30,19:44
 */
@Data
public class SpuBoundTo {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
