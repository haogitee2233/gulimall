package com.hao.common.to;

import lombok.Data;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName SkuHasStockVo.java
 * @createTime 2021/3/23,19:46
 * @Description sku是否有库存Vo
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
