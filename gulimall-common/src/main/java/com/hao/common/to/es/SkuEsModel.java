package com.hao.common.to.es;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenwenhao
 * @version 1.0.0
 * @ClassName SkuEsModel.java
 * @createTime 2021/3/21,16:17
 * @Description     商品上架发布时，数据保存到ES搜索服务中
 */
@Data
public class SkuEsModel {

    /**
     * sku信息
     */
    private Long skuId;
    private Long spuId;
    private String title;
    private BigDecimal skuPrice;
    private String skuImg;
    private Long saleCount;

    /**
     * 是否有库存
     */
    private Boolean hasStock;
    /**
     * 热度分
     */
    private Long hotScore;

    /**
     * 分类id，name
     */
    private Long catalogId;
    private String catalogName;

    /**
     * 品牌id，name，图片
     */
    private Long brandId;
    private String brandName;
    private String brandImg;


    /**
     * 商品规格属性信息
     */
    private List<Attrs> attrs;

    @Data
    public static class Attrs{
        private Long attrId;
        private String attrName;
        private String attrValue;
    }

}
